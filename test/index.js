'use strict';
const assert = require('assert');

const wrapper = require('..');
const options = 'http://localhost:5984/my-test-db';

describe('wrapper',function(){
	describe('passing options to service wrapper',function(){
		it('should pass options', function(done){
			let service = wrapper(options);
			service.start(null, function(err){
				if(err){
					done(err);
				}
				const client = service.getClient();
				assert.deepStrictEqual(client.config.db, 'my-test-db');
				done();
			});
		});
	});
	describe('When service has already been started',function(){
		it('#start(deps, callback) should not start a new client', function(done){
			const service = wrapper(options);
			service.start(null, function(err){
				if(err){
					done(err);
				}
				service.start(null, function(err){
					assert.deepStrictEqual(err.message , 'already started');
					done();
				});
			});
		});
	});
	describe('#stop', function(){
		it('should stop the service',function(done){
			let service = wrapper(options);
			service.start(null, function(){
				done();
				service.stop(function(err){
					if(err){
						done(err);
					}
					assert.equal(null, service.getClient());
				});
			});
		});
	});
	describe('On client initialization error',function(){
		it('should callback that error',function(done){
			const expected = {"Cannot read property 'url' of null": true, "Cannot read properties of null (reading 'url')": true};
			const badOptions = null; // url is required
			let service = wrapper(badOptions);
			service.start(null, function(err){
				assert(expected[err.message]);
				done();
			});
		});
	});
});
