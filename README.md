[![pipeline status](https://gitlab.com/runsvjs/couchdb/badges/master/pipeline.svg)](https://gitlab.com/runsvjs/couchdb/commits/master)
[![coverage report](https://gitlab.com/runsvjs/couchdb/badges/master/coverage.svg)](https://gitlab.com/runsvjs/couchdb/commits/master)
# runsv CouchDB (apache-nano client) service 

This is a service wrapper around the excellent [nano](https://github.com/apache/couchdb-nano) for [runsv](https://www.github.com/runsvjs/runsv).

## Install

If you have already installed `nano`  
```
$ npm install runsv-couchdb
```
Otherwise
```
$ npm install nano runsv-couchdb
```

> nano is a [peer dependency](https://nodejs.org/es/blog/npm/peer-dependencies/)


## Usage
```javascript
const runsv = require('runsv')();
const couchdb = require('runsv-couchdb')(/* nano config here*/);

runsv.addService(couchdb);
runsv.start(function(err, clients){
	const {couchdb} = clients;
	//your program goes here
});
```

## Configure nano

You can configure your connection to couchdb the [same way you](https://github.com/apache/couchdb-nano#configuration) you do it with `nano`. 

```javascript
// --- Configuration example comparison ---
//  Create client with nano
const nano = require('nano')('http://localhost:5984/my-db');
//  Create a service wrapper with runsv-couchdb
const runsvCouchdb = require('runsv-couchdb')('http://localhost:5984/my-db');

```
