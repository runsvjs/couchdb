'use strict';

const nano = require('nano');

function create(options){
	const name = 'couchdb';
	let client;

	function start(_, callback){
		if(client){
			return  callback(new Error('already started'));
		}
		try{
			client = nano(options);
		}
		catch(e){
			return callback(e);
		}
		return callback();
	}

	function stop(callback){
		client = null;
		return callback();
	}

	function getClient(){
		return client;
	}

	return Object.freeze({start, stop, name, getClient});
}

exports = module.exports = create;
